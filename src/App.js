import React, { Component } from "react";
import Header from "./components/common/Header";
import Movies from "./components/Movies";
import Movie from "./components/Movie";
import AddMovie from "./components/admin/AddMovie";
import AdminMovies from "./components/admin/AdminMovies";
import Login from "./components/Login";
import Error404 from "./components/Error404";
import "../src/assets/styles/bootstrap.min.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./assets/styles/index.scss";
import { createBrowserHistory } from "history";

const history = createBrowserHistory();

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <>
        <Router history={history}>
          <div className="wrapper" style={{ overflowX: "hidden" }}>
            <Route path="\/*(movies|movie)" component={Header} />
            <Switch>
              <Route path="/" exact component={Login} />
              <Route path="/login" exact component={Login} />
              <Route path="/movies/" exact component={Movies} />
              {/* for SEO makes more sense to have a movie/name path */}
              <Route path="/movie/:id" exact component={Movie} />
              <Route path="/admin/addMovie/" exact component={AddMovie} />
              <Route path="/admin/movies/" exact component={AdminMovies} />
              <Route path="*" exact component={Error404} />
            </Switch>
          </div>
        </Router>
      </>
    );
  }
}

export default App;
