import React from "react";
import { withAlert } from "react-alert";
import img404 from "../../assets/images/404.jpg";
import "./index.css";

const Error404 = ({ alert }) => {
  return (
    <section className="bg-half-150-60 bg-home d-flex align-items-center">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-6 col-md-12 text-center">
            <img src={img404} className="img-fluid" alt="404" />
          </div>

          <div className="col-lg-6 col-md-12 text-center">
            <div className="text-uppercase display-3 mt-30p">404</div>
            <div className="text-capitalize text-dark mb-4 error-page">Page not found, sorry</div>
            <p className="text-muted para-desc mx-auto"> Page doesn't seem to exist, check URL or contact us, please. </p>
            <a href="/movies" className="btn btn-primary mt-4 ml-2">
              Movies
            </a>
          </div>
        </div>
      </div>
    </section>
  );
};

export default withAlert()(Error404);
