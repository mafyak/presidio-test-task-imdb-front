import React, { useState } from "react";
import "./index.scss";
import openAPI from "../../service/UnsecuredAPI";
import UserService from "../../service/UserService";
import ValidationService from "../../service/ValidationService";
import { withAlert } from "react-alert";
import { withRouter } from "react-router-dom";

const Login = withRouter(({ alert, history }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  function onLoginSetSecret(token, refresh_token) {
    localStorage.setItem("logged in", "true");
    localStorage.setItem("token", token);
    localStorage.setItem("refresh_token", refresh_token);

    UserService.getUserInfo().then((user) => {
      const bcrypt = require("bcryptjs");
      const salt = bcrypt.genSaltSync(10);
      const encodedType = bcrypt.hashSync(user.data.type, salt);
      localStorage.setItem("type", encodedType);
      localStorage.setItem("fullName", user.data.fullName);
      localStorage.setItem("id", user.data.id);
      history.push("/movies");
    });
  }

  function callError(message) {
    alert.error(message);
  }

  function handleLogin(event) {
    event.preventDefault();
    if (ValidationService.validateEmail(email)) {
      var apiBaseUrl = "/oauth/token";
      var payload = {
        username: email,
        password: password,
        grant_type: "password",
      };
      const config = {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Basic b3VyVHJ1c3RlZENsaWVudDpjbGllbnRTZWNyZXQ=",
        },
      };
      const qs = require("querystring");
      var stringPayload = qs.stringify(payload);
      openAPI
        .post(apiBaseUrl, stringPayload, config)
        .then((response) => {
          if (response != null && response.status == 200) {
            onLoginSetSecret(response.data.access_token, response.data.refresh_token);
          }
        })
        .catch((error) => {
          if ("Network Error" == error.message) {
            callError("System is down, please try again in 10 minutes");
          } else {
            callError("Incorrect login/password");
          }
        });
    } else {
      callError("Incorrect email");
    }
  }

  function onPasswordChange({ target }) {
    setPassword(target.value);
  }

  function onEmailChange({ target }) {
    setEmail(target.value);
  }

  return (
    <section className="cover-user bg-white">
      <div className="container-fluid px-0">
        <div className="row g-0 position-relative">
          <div className="col-lg-4 cover-my-30 order-2">
            <div className="cover-user-img d-flex align-items-center">
              <div className="row">
                <div className="col-12">
                  <div className="card login-page border-0 z1">
                    <div className="card-body p-0">
                      <h4 className="card-title text-center">Hello, I'm Bob. And you are?</h4>
                      <form className="login-form mt-4" onSubmit={handleLogin}>
                        <div className="row">
                          <div className="col-lg-12">
                            <div className="mb-3">
                              <label className="form-label">
                                Your Email <span className="text-danger">*</span>
                              </label>
                              <div className="form-icon position-relative">
                                <input type="email" className="form-control" placeholder="Email" name="email" required="" onChange={onEmailChange} />
                              </div>
                            </div>
                          </div>

                          <div className="col-lg-12">
                            <div className="mb-3">
                              <label className="form-label">
                                Password <span className="text-danger">*</span>
                              </label>
                              <div className="form-icon position-relative">
                                <input type="password" className="form-control" placeholder="Password" required="" onChange={onPasswordChange} />
                              </div>
                            </div>
                          </div>

                          <div className="col-lg-12 mb-0">
                            <div className="d-grid">
                              <button className="btn btn-primary">Sign in</button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-8 offset-lg-4 padding-less img order-1 image-backgr" data-jarallax='{"speed": 0.5}'></div>
        </div>
      </div>
    </section>
  );
});

export default withAlert()(Login);
