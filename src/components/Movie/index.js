import React, { useState, useEffect } from "react";
import MovieService from "../../service/MovieService"; // @todo update to use @services
import ReviewService from "../../service/ReviewService";
import AdminService from "../../service/AdminService";
import { withAlert } from "react-alert";
import Ava01 from "../../assets/images/avatar_default.jpeg";
import Scene1 from "../../assets/images/scene1.jpg";
import Scene2 from "../../assets/images/scene2.jpg";
import Scene3 from "../../assets/images/scene3.jpg";
import Scene4 from "../../assets/images/scene4.jpg";
import Scene5 from "../../assets/images/scene5.jpg";
import LikeButton from "../common/LikeButton";
import "./index.scss";

const Movie = ({ alert, match }) => {
  const ratingNumbers = [5, 4, 3, 2, 1];
  const ratingNames = ["Terrible", "Bad", "Ok", "Good", "Great"];
  const [id, setId] = useState(match.params.id);
  const [movie, setMovie] = useState(null);
  // const [numbers, setMovie] = useState(null);
  const [reviewButtonTitle, setReviewButtonTitle] = useState("Add review");
  const [toSave, setToSave] = useState(true);
  const [user, setUser] = useState();
  const [reviewId, setReviewId] = useState();
  const [currentUserId, setCurrentUserId] = useState(parseInt(localStorage.getItem("id"), 10));
  const [rating, setRating] = useState(-1);
  const [comment, setComment] = useState("");

  useEffect(() => {
    async function refreshMovie() {
      const response = await MovieService.getMovieById(id);
      setMovie(response.data);
    }
    if (!movie) {
      refreshMovie();
    }
  }, []);

  async function refreshMovie() {
    const response = await MovieService.getMovieById(id);
    setRating(-1);
    setComment("");
    setReviewButtonTitle("Add review");
    setMovie(response.data);
    setToSave(true);
  }

  function printActors() {
    return movie.cast.map((actor, index) => (
      <li className={(index % 2 == 0 ? "dark-bg" : "") + " mb-1"} key={actor.id}>
        <img src={Ava01} className="avatar avatar-md-sm rounded-circle shadow mr5" alt={actor.fullName} />
        {actor.fullName}
      </li>
    ));
  }

  function printEditButton(review) {
    if (currentUserId == review.user.id) {
      return (
        <a className="pe-3" href="#Review" onClick={(e) => updateSaveButton(review)}>
          <i className="mdi mdi-reply"></i> Edit
        </a>
      );
    }
  }

  function printDeleteButton(review) {
    if (currentUserId == review.user.id || AdminService.checkIfAdmin()) {
      return (
        <>
          <br />
          <a className="pe-3" onClick={(e) => deleteReviewButton(review)}>
            <i className="mdi mdi-reply"></i> Delete
          </a>
        </>
      );
    }
  }

  function deleteReviewButton(review) {
    ReviewService.deleteReview(review.id).then(() => refreshMovie());
  }

  function updateSaveButton(review) {
    setRating(review.rating);
    setComment(review.summary);
    setReviewButtonTitle("Update Review");
    setToSave(false);
    setUser(review.user);
    setReviewId(review.id);
  }

  function onRatingChange({ target }) {
    setRating(target.value);
  }

  // @todo add pagination
  // @todo extract to separate component
  function printReviews() {
    if (movie.reviews && movie.reviews.length > 0) {
      return movie.reviews.map((review) => (
        <li key={review.id} className="mt-4">
          <div className="d-flex justify-content-between">
            <div className="d-flex align-items-center">
              <a className="pe-3" href="/">
                <img src={review.user.avatar} className="img-fluid avatar avatar-md-sm rounded-circle shadow" alt="img" />
              </a>
              <div className="flex-1 commentor-detail">
                <h6 className="mb-0">
                  <a href="/" className="media-heading text-dark">
                    {review.user.fullName}
                  </a>
                </h6>
                <small className="text-muted">{"Date: " + review.creationDate}</small>
                <br />
                <small className="text-muted">{"Rating: " + review.rating}</small>
              </div>
            </div>

            <div className="d-flex align-items-center">
              <div className="flex-1 commentor-detail">
                <LikeButton rLikes={review.likes} reviewId={review.id} currentUserId={currentUserId} />
                <br />
                <a className="pe-3" href="/">
                  <i className="mdi mdi-reply"></i> Reply
                </a>
                <br />
                {printEditButton(review)}
                {printDeleteButton(review)}
              </div>
            </div>
          </div>

          <div className="mt-3">
            <p className="text-muted fst-italic p-3 bg-light rounded">{review.summary}</p>
          </div>
        </li>
      ));
    }
  }

  function onCommentChange({ target }) {
    setComment(target.value);
  }

  function callError(message) {
    alert.error(message);
  }

  // Weird works fast when user is not logged in, but slow when user is logged in. O_o
  function printStars() {
    return ratingNumbers.map((e, i) => (
      <React.Fragment key={"rating-" + e}>
        <input type="radio" id={"rating-" + e} name="rating" value={e} onClick={() => setRating(e)} />
        <label className={"ratingControl-stars ratingControl-stars--" + e} htmlFor={"rating-" + e}>
          {e}
        </label>
      </React.Fragment>
    ));
  }

  function addReview(e) {
    e.preventDefault();
    if (rating == -1) {
      callError("Please set rating");
    } else {
      let review = {};
      review.id = reviewId;
      review.summary = comment;
      review.rating = localStorage.getItem("rating");
      review.user = user;

      let theMovie = {};
      theMovie.id = movie.id;
      review.movie = theMovie;
      if (toSave) {
        ReviewService.addReview(review).then((response) => {
          refreshMovie();
        });
      } else {
        ReviewService.updateReview(review).then(() => refreshMovie());
      }
    }
  }

  if (!movie) {
    return <div>Loading...</div>;
  } else {
    return (
      <>
        <section className="bg-half-150-60 bg-light d-table w-100">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-lg-12 text-center">
                <div className="page-next-level">
                  <h4 className="title"> {movie.title} </h4>
                  <ul className="list-unstyled mt-4">
                    <li className="list-inline-item h6 user text-muted me-2">
                      <span className="text-dark">Genre :</span> {movie.genre.map((genre) => genre.name + " ")}
                    </li>
                    <li className="list-inline-item h6 date text-muted">
                      <span className="text-dark">Release Date :</span> {movie.releaseDate}
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>

        <div className="position-relative">
          <div className="shape overflow-hidden text-white">
            <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
            </svg>
          </div>
        </div>

        <section className="section">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-12 text-center">
                <img src={Scene5} className="img-fluid mt30 rounded" alt="" />
              </div>

              <div className="col-md-10 mt-4 pt-2">
                <div className="bg-light rounded p-4">
                  <h5 className="card-title border-bottom pb-3 mb-3">Plot :</h5>
                  <p className="text-muted fst-italic mb-0">" {movie.plot} "</p>
                </div>

                <div className="row">
                  <div className="col-md-6 mt-4 pt-2">
                    <img src={Scene4} className="img-fluid rounded" alt="" />
                  </div>

                  <div className="col-md-6 mt-4 pt-2">
                    <img src={Scene3} className="img-fluid rounded" alt="" />
                  </div>
                </div>

                <div className="bg-light rounded p-4 mt-4 pt-2">
                  <h5 className="card-title border-bottom pb-3 mb-3">Cast :</h5>

                  <ul className="list-unstyled text-muted mt-4">{printActors()}</ul>
                </div>

                <div className="row align-items-center">
                  <div className="col-lg-6 mt-4 pt-2">
                    <div className="card work-details rounded bg-light border-0">
                      <div className="card-body">
                        <h5 className="card-title border-bottom pb-3 mb-3">Movie Details :</h5>
                        <dl className="row mb-0">
                          <dt className="col-md-4 col-5">Release Date :</dt>
                          <dd className="col-md-8 col-7 text-muted">{movie.releaseDate ? movie.releaseDate : "Unknown"}</dd>

                          <dt className="col-md-4 col-5">Language :</dt>
                          <dd className="col-md-8 col-7 text-muted">{movie.lang}</dd>

                          <dt className="col-md-4 col-5">Rating :</dt>
                          <dd className="col-md-8 col-7 text-muted">{movie.rating.toFixed(2)}</dd>
                        </dl>
                      </div>
                    </div>
                  </div>

                  <div className="col-lg-6 mt-4 pt-2">
                    <img src={Scene2} className="rounded h207" alt="" />
                  </div>
                </div>

                <div className="card shadow rounded border-0 mt-4">
                  <div className="card-body">
                    <h5 className="card-title mb-0">Comments :</h5>

                    <ul className="media-list list-unstyled mb-0">{printReviews()}</ul>
                  </div>
                </div>

                <div className="card shadow rounded border-0 mt-4">
                  <div className="card-body">
                    <h5 className="card-title mb-0">{reviewButtonTitle} :</h5>

                    <form className="mt-3" onSubmit={addReview}>
                      <div className="row">
                        <div className="col-md-12 mb-3">
                          <div className="col-lg-4 pt-2" id="Review">
                            <label className="form-label">Rating</label>

                            <div className="rating-frame">
                              <div className="rating-name">{ratingNames[rating - 1]}</div>
                              <div className="ratingControl">{printStars()}</div>
                            </div>
                          </div>
                        </div>

                        <div className="col-md-12">
                          <div className="mb-3">
                            <label className="form-label">Your Comment</label>
                            <div className="form-icon position-relative">
                              <textarea
                                id="message"
                                placeholder="Your Comment"
                                rows="5"
                                name="message"
                                className="form-control"
                                required=""
                                value={comment}
                                onChange={onCommentChange}
                              ></textarea>
                            </div>
                          </div>
                        </div>

                        <div className="col-md-12">
                          <div className="send d-grid">
                            <button type="submit" className="btn btn-primary">
                              {reviewButtonTitle}
                            </button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </>
    );
  }
};

export default withAlert()(Movie);
