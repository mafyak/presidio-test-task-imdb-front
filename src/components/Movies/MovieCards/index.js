import React from "react";
import { withAlert } from "react-alert";

const MovieCards = ({ alert, movies }) => {
  function printRating(rating) {
    if (rating) {
      return (
        <li className="h6">
          <span className="text-muted">Rating :</span> {rating.toFixed(2)}
        </li>
      );
    }
  }

  function printGenre(genre) {
    if (genre && genre.length > 0) {
      return (
        <li className="h6">
          <span className="text-muted">Genre :</span> {genre.map((genre) => genre.name + " ")}
        </li>
      );
    }
  }

  if (movies && movies.length > 0) {
    return movies.map((movie) => (
      <div key={movie.id} className="col-lg-3 col-md-6 col-12 mt-4 pt-2">
        <div className="company-list card border-0 rounded shadow bg-white">
          <div className="text-center border-bottom">
            <img src={movie.image} className="movie movie-small mx-auto d-block" alt={movie.title} />
          </div>
          <div className="p-4">
            <ul className="list-unstyled mb-4">
              <li className="h6">
                <a href={"/movie/" + movie.id} className="text-dark h5 name">
                  {movie.title}
                </a>
              </li>
              {printGenre(movie.genre)}
              <li className="h6">
                <span className="text-muted">Released :</span> {movie.releaseDate}
              </li>
              {printRating(movie.rating)}
              <li className="h6">
                <span className="text-muted">Language :</span> {movie.lang}
              </li>
            </ul>
            <div className="d-grid">
              <a href={"/movie/" + movie.id} className="btn btn-soft-primary">
                View Detail <i className="uil uil-angle-right-b align-middle"></i>
              </a>
            </div>
          </div>
        </div>
      </div>
    ));
  } else {
    return <></>;
  }
};

export default withAlert()(MovieCards);
