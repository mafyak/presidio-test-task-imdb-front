import React, { useState, useEffect } from "react";
import MovieService from "../../service/MovieService"; // @todo update to use @services
import MovieSearchWidget from "../common/MovieSearchWidget";
import MoviePagination from "../common/MoviePagination";
import MovieCards from "./MovieCards";
import { withAlert } from "react-alert";

const Movies = ({ alert }) => {
  const [movies, setMovies] = useState([]);
  const [title, setTitle] = useState(null);
  const [genre, setGenre] = useState(null);
  const [perPage, setPerPage] = useState(); // used in dropdown. Updated upon dropdown pick
  const [size, setSize] = useState(); // used in pagination. Updated upon search click
  const [sortBy, setSortBy] = useState(null);
  const [actorId, setActorId] = useState(null);
  const [pageNumber, setPageNumber] = useState(0);
  const [totalPages, setTotalPages] = useState(0);
  const [first, setFirst] = useState(true);
  const [last, setLast] = useState(false);
  const [releasedBeforeDate, setReleasedBeforeDate] = useState(null);
  const [releasedAfterDate, setReleasedAfterDate] = useState(null);

  useEffect(() => {
    async function refreshMovies() {
      updateAllMovies(0, 12, null, null, null, null, null, null);
    }
    if (movies.length < 1) {
      refreshMovies();
    }
  }, []);

  function updateMovies(e) {
    e.preventDefault();
    updateAllMovies(0, perPage, title, genre, actorId, sortBy, releasedBeforeDate, releasedAfterDate);
  }

  async function updateAllMovies(page, size, title, genre, actorId, sortBy, releasedBeforeDate, releasedAfterDate) {
    const response = await MovieService.retrieveFilteredPaginableMovies(
      page,
      size,
      title,
      genre,
      actorId,
      sortBy,
      releasedBeforeDate,
      releasedAfterDate
    );
    setMovies(response.data.content);
    setPageNumber(response.data.pageable.pageNumber);
    setSize(response.data.pageable.pageSize);
    setPerPage(response.data.pageable.pageSize);
    setTotalPages(response.data.totalPages);
    setFirst(response.data.first);
    setLast(response.data.last);
  }

  function getPaginableCompanies(page, size) {
    updateAllMovies(page, size, title, genre, actorId, sortBy, releasedBeforeDate, releasedAfterDate);
  }

  function onChangeDirection({ target }) {
    setSortBy(target.value);
  }

  function onMovieCountChange({ target }) {
    setPerPage(target.value);
  }

  function onTitleChange({ target }) {
    setTitle(target.value);
  }

  function changeGenreId({ target }) {
    setGenre(target.value);
  }

  function changeActorId({ target }) {
    setActorId(target.value);
  }

  return (
    <>
      <section className="bg-half-150-60 bg-light d-table w-100">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-12 text-center">
              <div className="page-next-level">
                <h4 className="title"> All movies </h4>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div className="position-relative">
        <div className="shape overflow-hidden text-white">
          <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
          </svg>
        </div>
      </div>

      <section className="section">
        <div className="container">
          <MovieSearchWidget
            updateMovies={updateMovies}
            onTitleChange={onTitleChange}
            changeGenreId={changeGenreId}
            onChangeDirection={onChangeDirection}
            changeActorId={changeActorId}
            onMovieCountChange={onMovieCountChange}
          />
          <div className="row">
            <MovieCards movies={movies} />
            <MoviePagination
              first={first}
              pageNumber={pageNumber}
              size={size}
              totalPages={totalPages}
              getPaginableCompanies={getPaginableCompanies}
              last={last}
            />
          </div>
        </div>
      </section>
    </>
  );
};

export default withAlert()(Movies);
