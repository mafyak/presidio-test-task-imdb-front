import React, { useState, useEffect } from "react";
import MovieService from "../../../service/MovieService"; // @todo update to use @services
import { withAlert } from "react-alert";

import "./index.css";

const AddMovie = ({ alert }) => {
  const [title, setTitle] = useState("");
  const [plot, setPlot] = useState("");
  const [releaseDate, setReleaseDate] = useState("");
  const [lang, setLang] = useState("");
  const [image, setImage] = useState("");
  const [cast, setCast] = useState([]);
  const [genre, setGenre] = useState([]);

  function onTitleChange({ target }) {
    setTitle(target.value);
  }

  function onLanguageChange({ target }) {
    setLang(target.value);
  }

  function onReleaseDateChange({ target }) {
    setReleaseDate(target.value);
  }

  function onPlotChange({ target }) {
    setPlot(target.value);
  }

  function saveMovie() {
    let movie = {};
    movie.title = title;
    movie.plot = plot;
    movie.lang = lang;
    movie.releaseDate = releaseDate;
    MovieService.addMovie(movie).then((response) => {
      alertSuccess("Movie was successfully saved.");
      setTitle("");
      setPlot("");
      setReleaseDate("");
      setLang("");
    });
  }

  function alertError(message) {
    alert.error(message);
  }

  function alertSuccess(message) {
    alert.success(message);
  }

  return (
    <>
      <section className="bg-half-150-60 bg-light d-table w-100">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-12 text-center">
              <div className="page-next-level">
                <h4 className="title"> Add movie </h4>
              </div>
            </div>
          </div>
        </div>
      </section>

      <div className="position-relative">
        <div className="shape overflow-hidden text-white">
          <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
          </svg>
        </div>
      </div>

      <section className="section">
        <div className="container">
          <div className="row">
            <div className="col-md-2 mt-4 pt-2"></div>
            <div className="col-md-8 col-12 mt-4 pt-2">
              <div className="tab-pane fade bg-white shadow rounded p-4 active show" id="account" role="tabpanel" aria-labelledby="account-details">
                <form onSubmit={saveMovie}>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="mb-3">
                        <label className="form-label">Title</label>
                        <div className="form-icon position-relative">
                          <input name="name" id="title" type="text" className="form-control" onChange={onTitleChange} />
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="mb-3">
                        <label className="form-label">ReleaseDate</label>
                        <div className="form-icon position-relative">
                          <input name="name" id="releaseDate" type="date" className="form-control" onChange={onReleaseDateChange} />
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="mb-3">
                        <label className="form-label">Language</label>
                        <div className="form-icon position-relative">
                          <input name="name" id="language" type="text" className="form-control" onChange={onLanguageChange} />
                        </div>
                      </div>
                    </div>
                    <div className="col-md-12">
                      <div className="mb-3">
                        <label className="form-label">Plot</label>
                        <div className="form-icon position-relative">
                          <textarea name="name" id="plot" type="textarea" className="form-control" onChange={onPlotChange} />
                        </div>
                      </div>
                    </div>

                    <div className="col-lg-12 mt-2 mb-0">
                      <button className="btn btn-primary">Save Changes</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default withAlert()(AddMovie);
