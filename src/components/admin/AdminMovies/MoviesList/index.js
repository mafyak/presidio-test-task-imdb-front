import React from "react";
import { withAlert } from "react-alert";
import MovieService from "../../../../service/MovieService"; // @todo update to use @services
import "./index.scss";

// Update after removal doesn't update list. Why? @todo
const MovieList = ({ alert, movies, updateAllMoviesAfterRemove }) => {
  function printTableRows() {
    if (movies) {
      return movies.map((movie) => (
        <tr key={movie.id} value={movie.id}>
          <td scope="row">{movie.id}</td>
          <td>
            <div className="d-flex align-items-center">
              <a href={"/movie/" + movie.id} className="mb-0 font-weight-normal h5">
                {movie.title}
              </a>
            </div>
          </td>
          <td>{movie.releaseDate}</td>
          <td>{movie.lang}</td>
          <td>
            <img className="miniImage" src={movie.image} alt="movie pic" />
          </td>
          <td>{movie.rating.toFixed(2)}</td>
          <td>{printGenres(movie.genre)}</td>
          <td>
            <button type="button" className="btn btn-primary mg5p">
              Edit
            </button>
            <button type="button" className="btn btn-primary mg5p" onClick={() => removeMovie(movie)}>
              Remove
            </button>
          </td>
        </tr>
      ));
    }
  }

  function alertSuccess(message) {
    alert.success(message);
  }

  function removeMovie(movie) {
    MovieService.deleteMovie(movie.id)
      .then(() => alertSuccess("Movie is removed"))
      .then(() => updateAllMoviesAfterRemove);
  }

  function printGenres(genres) {
    if (genres) return genres.map((genre) => <span key={genre.id}>{genre.name + " "}</span>);
  }

  if (movies && movies.length > 0) {
    return (
      <section className="section border-top">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-lg-12">
              <div className="table-responsive movie-table bg-white shadow rounded">
                <table className="table mb-0 table-center">
                  <thead>
                    <tr>
                      <th scope="col">Id</th>
                      <th scope="col">Title</th>
                      <th scope="col">ReleaseDate</th>
                      <th scope="col">Lang</th>
                      <th scope="col">Image</th>
                      <th scope="col">Rating</th>
                      <th scope="col">Genre</th>
                      <th scope="col" style={{ width: "100px" }}>
                        Action
                      </th>
                    </tr>
                  </thead>
                  <tbody>{printTableRows()}</tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  } else {
    return <></>;
  }
};

export default withAlert()(MovieList);
