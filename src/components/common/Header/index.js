import React, { useState, useEffect } from "react";
import { withAlert } from "react-alert";
import AdminService from "../../../service/AdminService";

const Header = ({ alert }) => {
  const [isLoggedIn, setIsLoggedIn] = useState(localStorage.getItem("logged in"));
  const [scroll, setScroll] = useState(0);

  useEffect(() => {
    document.addEventListener("scroll", () => {
      const scrollCheck = window.scrollY < 10;
      if (scrollCheck !== scroll) {
        setScroll(scrollCheck);
      }
    });
  });

  function logOut(e) {
    localStorage.clear();
  }

  function printAdminMenu() {
    if (AdminService.checkIfAdmin()) {
      return (
        <li className="has-submenu parent-menu-item">
          <a href="/">Admin</a>
          <span className="menu-arrow"></span>
          <ul className="submenu">
            <li>
              <a href="/admin/addMovie/" className="sub-menu-item">
                Add Movie
              </a>
            </li>
            {/* <li>
              <a href="/movies" className="sub-menu-item">
                Edit Movie
              </a>
            </li> */}
            <li>
              <a href="/admin/movies/" className="sub-menu-item">
                Movie List
              </a>
            </li>
          </ul>
        </li>
      );
    }
  }

  return (
    <div>
      <header id="topnav" className={"defaultscroll sticky" + (scroll ? "" : " nav-sticky")}>
        <div className="container">
          <div className={"buy-button" + (isLoggedIn ? "" : " d-none")}>
            <a href="/" onClick={logOut}>
              <div className="btn btn-primary login-btn-primary">Log Out</div>
              <div className="btn btn-light login-btn-light">Log Out</div>
            </a>
          </div>
          <div id="navigation">
            <ul className="navigation-menu nav-light">
              <li>
                <a href="/movies" className="sub-menu-item">
                  Home
                </a>
              </li>

              <li className="has-submenu parent-menu-item">
                <a href="/">Pages</a>
                <span className="menu-arrow"></span>
                <ul className="submenu">
                  <li>
                    <a href="/movies" className="sub-menu-item">
                      Movies
                    </a>
                  </li>
                </ul>
              </li>
              {printAdminMenu()}
            </ul>
          </div>
        </div>
      </header>
    </div>
  );
};

export default withAlert()(Header);
