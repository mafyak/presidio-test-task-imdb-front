import React, { useState, useEffect } from "react";
import ReviewService from "../../../service/ReviewService";

const LikeButton = ({ reviewId, rLikes, currentUserId }) => {
  const [likes, setLikes] = useState(rLikes);
  const [changed, setChanged] = useState(false);

  useEffect(() => {
    setChanged(false);
  }, [changed]);

  function doLike() {
    if (likes.includes(currentUserId)) {
      var index = likes.indexOf(currentUserId);
      likes.splice(index, 1);
      ReviewService.removeLike(reviewId);
    } else {
      likes.push(currentUserId);
      ReviewService.addLike(reviewId);
    }
    setLikes(likes);
    setChanged(true);
  }

  return (
    <a className={"btn btn-icon btn-pills mt-2" + (likes.includes(currentUserId) ? " btn-danger" : " btn-outline-danger")} onClick={() => doLike()}>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill="none"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
        className="feather feather-heart icons"
      >
        <path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path>
      </svg>
      {likes.length}
    </a>
  );
};
export default LikeButton;
