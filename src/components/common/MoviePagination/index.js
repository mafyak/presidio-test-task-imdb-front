import React, { useState, useEffect } from "react";
import GenreService from "../../../service/GenreService"; // @todo update to use @services
import ActorService from "../../../service/ActorService";
import { withAlert } from "react-alert";

const MoviePagination = ({ alert, first, pageNumber, size, totalPages, getPaginableCompanies, last }) => {
  const [genreList, setGenreList] = useState([]);
  const [actorList, setActorList] = useState([]);

  useEffect(() => {
    async function refreshGenreList() {
      const response = await GenreService.getAllGenres();
      setGenreList(response.data);
    }
    async function refreshActorList() {
      const response = await ActorService.getAllActors();
      setActorList(response.data);
    }
    if (genreList.length < 1) {
      refreshGenreList();
    }
    if (actorList.length < 1) {
      refreshActorList();
    }
  }, []);

  if (totalPages) {
    return (
      <div className="d-flex p-4 justify-content-center">
        <ul className="pagination mb-0">
          <li className="page-item">
            <button
              className="page-link"
              key="prev"
              aria-label="Previous"
              onClick={() => {
                if (!first) getPaginableCompanies(pageNumber - 1, size);
              }}
            >
              Back
            </button>
          </li>
          {Array.from(Array(totalPages), (e, i) => (
            <li className={"page-item" + (pageNumber == i ? " active" : "")} key={"page" + i}>
              <button className="page-link" onClick={() => getPaginableCompanies(i, size)}>
                {i + 1}
              </button>
            </li>
          ))}
          <li className="page-item">
            <button
              className="page-link"
              key="next"
              aria-label="Next"
              onClick={() => {
                if (!last) getPaginableCompanies(pageNumber + 1, size);
              }}
            >
              Next
            </button>
          </li>
        </ul>
      </div>
    );
  } else {
    return <></>;
  }
};

export default withAlert()(MoviePagination);
