import React, { useState, useEffect } from "react";
import GenreService from "../../../service/GenreService"; // @todo update to use @services
import ActorService from "../../../service/ActorService";
import { withAlert } from "react-alert";

const MoviesSearchWidget = ({ alert, updateMovies, onTitleChange, changeGenreId, onChangeDirection, changeActorId, onMovieCountChange }) => {
  const [genreList, setGenreList] = useState([]);
  const [actorList, setActorList] = useState([]);

  useEffect(() => {
    async function refreshGenreList() {
      const response = await GenreService.getAllGenres();
      setGenreList(response.data);
    }
    async function refreshActorList() {
      const response = await ActorService.getAllActors();
      setActorList(response.data);
    }
    if (genreList.length < 1) {
      refreshGenreList();
    }
    if (actorList.length < 1) {
      refreshActorList();
    }
  }, []);

  return (
    <form role="search" className="mt-4 d-flex flex-column align-items-stretch" onSubmit={updateMovies}>
      <div className="widget mt-4 row">
        <div className="col-12 col-sm-4 col-md-3 col-lg-3 pb-4">
          <div className="form-group mb-0 ">
            <input
              type="text"
              className="form-control mb-4 align-self-center"
              name="s"
              id="s"
              placeholder="Type movie title here"
              onChange={onTitleChange}
            />
          </div>
        </div>

        <div className="col-12 col-sm-4 col-md-3 col-lg-2 pb-4">
          <div className="form-group mb-0 ">
            <select className="p-2 form-control custom-select" id="job-catagories" onChange={changeGenreId}>
              <option key={null} value={""}>
                Any genre
              </option>
              {genreList.map((genre) => (
                <option key={genre.id} value={genre.id}>
                  {genre.name}
                </option>
              ))}
            </select>
          </div>
        </div>

        <div className="col-12 col-sm-4 col-md-3 col-lg-2 pb-4">
          <div className="form-group mb-0 ">
            <select className="p-2 form-control custom-select" id="job-catagories" onChange={changeActorId}>
              <option value={""}>Any actor</option>
              {actorList.map((actor) => (
                <option key={actor.id} value={actor.id}>
                  {actor.fullName}
                </option>
              ))}
            </select>
          </div>
        </div>

        <div className="col-12 col-sm-4 col-md-3 col-lg-1 pb-4">
          <div className="form-group mb-0 ">
            <select className="p-2 form-control custom-select" id="job-catagories" defaultValue={"12 movies"} onChange={onMovieCountChange}>
              <option value={12}>12 movies</option>
              <option value={24}>24 movies</option>
            </select>
          </div>
        </div>

        <div className="col-12 col-sm-4 col-md-3 col-lg-3 pb-4">
          <div className="form-group mb-0 ">
            <select className="p-2 form-control custom-select" id="job-catagories" defaultValue={"rating,asc"} onChange={onChangeDirection}>
              <option value={" "}>Order by</option>
              <option value={"rating,asc"}>Low rating first</option>
              <option value={"rating,desc"}>High rating first</option>
            </select>
          </div>
        </div>

        <div className="col-12 col-sm-4 col-md-3 col-lg-1 pb-4">
          <div className="form-group mb-0 ">
            <button className="btn btn-primary align-self-end"> Search </button>
          </div>
        </div>
      </div>
    </form>
  );
};

export default withAlert()(MoviesSearchWidget);
