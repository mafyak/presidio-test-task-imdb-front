import openAPI from "./UnsecuredAPI";
import api from "./SecuredAPI";

const ACTOR_API_URL = "/api/actors";

class ActorService {
  getActor(id) {
    return api.get(`${ACTOR_API_URL}/${id}`);
  }

  getAllActors() {
    return openAPI.get(`${ACTOR_API_URL}`);
  }

  saveActor(actor) {
    return api.post(`${ACTOR_API_URL}`, actor);
  }

  updateActor(actor) {
    return api.put(`${ACTOR_API_URL}/`, actor);
  }

  deleteActor(id) {
    return api.delete(`${ACTOR_API_URL}/${id}`);
  }
}

export default new ActorService();
