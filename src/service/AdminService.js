class AdminService {
  static checkIfAdmin() {
    var type = localStorage.getItem("type");
    if (type) {
      const bcrypt = require("bcryptjs");
      return bcrypt.compareSync("ADMIN", type);
    }
  }
}

export default AdminService;
