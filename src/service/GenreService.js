import openAPI from "./UnsecuredAPI";
import api from "./SecuredAPI";

const GENRE_API_URL = "/api/genres";

class GenreService {
  getGenre(id) {
    return api.get(`${GENRE_API_URL}/${id}`);
  }

  getAllGenres() {
    return openAPI.get(`${GENRE_API_URL}`);
  }

  saveGenre(genre) {
    return api.post(`${GENRE_API_URL}`, genre);
  }

  updateGenre(genre) {
    return api.put(`${GENRE_API_URL}/`, genre);
  }

  deleteGenre(id) {
    return api.delete(`${GENRE_API_URL}/${id}`);
  }
}

export default new GenreService();
