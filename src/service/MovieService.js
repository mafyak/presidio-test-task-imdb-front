import api from "./SecuredAPI";
import openAPI from "./UnsecuredAPI";

const MOVIE_API_URL = "/api/movies";

class MovieService {
  retrieveAllMovies() {
    return openAPI.get(`${MOVIE_API_URL}`);
  }

  retrieveFilteredByNameMovies(page, size, MovieName) {
    return openAPI.get(`${MOVIE_API_URL}?page=` + page + `&size=` + size + `&MovieName=` + MovieName);
  }

  retrieveFilteredPaginableMovies(page, size, title, genre, actorId, sortBy, releasedBeforeDate, releasedAfterDate) {
    return openAPI.get(
      `${MOVIE_API_URL}?page=` +
        page +
        `&size=` +
        size +
        (sortBy ? `&sort=` + sortBy : ``) +
        (title ? `&title=` + title : ``) +
        (genre ? `&genre=` + genre : ``) +
        (actorId ? `&actorId=` + actorId : ``) +
        (releasedBeforeDate ? `&releasedBeforeDate=` + releasedBeforeDate : ``) +
        (releasedAfterDate ? `&releasedAfterDate=` + releasedAfterDate : ``)
    );
  }

  deleteMovie(id) {
    return api.delete(`${MOVIE_API_URL}/${id}`);
  }

  getMovieById(id) {
    return openAPI.get(`${MOVIE_API_URL}/${id}`);
  }

  updateMovie(movie) {
    return api.put(`${MOVIE_API_URL}`, movie);
  }

  addMovie(movie) {
    return api.post(`${MOVIE_API_URL}`, movie);
  }
}

export default new MovieService();
