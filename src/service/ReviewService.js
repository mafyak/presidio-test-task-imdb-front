import api from "./SecuredAPI";
import openAPI from "./UnsecuredAPI";

const REVIEW_API_URL = "/api/reviews";

class ReviewService {
  retrieveAllReviews() {
    return openAPI.get(`${REVIEW_API_URL}`);
  }

  deleteReview(id) {
    return api.delete(`${REVIEW_API_URL}/${id}`);
  }

  getReviewById(id) {
    return openAPI.get(`${REVIEW_API_URL}/${id}`);
  }

  updateReview(review) {
    return api.put(`${REVIEW_API_URL}`, review);
  }

  removeLike(reviewId) {
    return api.put(`${REVIEW_API_URL}/removeLike/${reviewId}`, reviewId);
  }

  addLike(reviewId) {
    return api.put(`${REVIEW_API_URL}/addLike/${reviewId}`, reviewId);
  }

  addReview(review) {
    return api.post(`${REVIEW_API_URL}`, review);
  }
}

export default new ReviewService();
