import axios from "axios";
import AuthService from "./AuthService";

const api = axios.create({
  baseURL: "http://localhost:9000",
});

api.interceptors.request.use((config) => {
  const token = AuthService.getAccessToken();
  if (token) {
    config.headers.Authorization = "Bearer " + token;
  }
  return config;
});

api.interceptors.response.use(
  (resp) => {
    return resp;
  },
  (err) => {
    const error = err.response;
    if (error) {
      // if error is 401
      // @todo find out how to hide a 401 error that can be seen in console?
      if (error.status === 401 && error.config && !error.config.__isRetryRequest) {
        return AuthService.refreshToken().then((response) => {
          if (response) {
            error.config.__isRetryRequest = true;
            return api.request(error.config).then((response) => {
              return response;
            });
          }
        });
      }
    }
  }
);

export default api;
