import axios from "axios";

const openAPI = axios.create({
  baseURL: "http://localhost:9000",
});

export default openAPI;
