import api from "./SecuredAPI";
import openAPI from "./UnsecuredAPI";

const USER_API_URL = "/api/users";
const USER_API_URL_GET_INFO = "/api/users/userInfo";

class UserService {
  registerUser(user) {
    return api.post(`${USER_API_URL}`, user);
  }

  updateUser(user) {
    return api.put(`${USER_API_URL}`, user);
  }

  getUserInfo() {
    return api.get(USER_API_URL_GET_INFO);
  }
}

export default new UserService();
